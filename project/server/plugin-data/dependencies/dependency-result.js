
'use strict';


class DependencyResult {
  constructor(name, versionCurrent, versionRequired, type, license, licenses, parent) {
    this.name = name;
    this.versionLatest = '';
    this.versionActorJs = '';
    this.versionWanted = '';
    this.versionCurrent = versionCurrent;
    this.versionRequired = [versionRequired];
    this.differActorJs = false;
    this.differWanted = false;
    this.differCurrent = false;
    this.type = type;
    this.license = license;
    this.licenses = licenses;
    this.parents = parent ? [parent] : [];
    this.dependencies = [];
  }
  
  addParentUniqueVersionRequired(parent, versionRequired) {
    this.parents.push(parent.name);
    if(-1 === this.versionRequired.indexOf(versionRequired)) {
      this.versionRequired.push(versionRequired);
    }
  }
  
  addVersions(versionWanted, versionLatest) {
    this.versionWanted = versionWanted;
    this.versionLatest = versionLatest;
  }
  
  addLicenses(licenses) {
    this.licenses = licenses;
  }
}

module.exports = DependencyResult;
