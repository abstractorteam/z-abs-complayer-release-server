
'use strict';

const ActorPathData = require('z-abs-corelayer-server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class DependenciesGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.asynchReadFileResponse(ActorPathData.getDependenciesFile());
  }
}

module.exports = DependenciesGet;
