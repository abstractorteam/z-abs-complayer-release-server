
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class DependenciesAnalyzeGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.asynchReadFileResponse(ActorPathGenerated.getDependenciesFile());
  }
}

module.exports = DependenciesAnalyzeGet;
