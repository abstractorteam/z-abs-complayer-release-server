
'use strict';

const DependencyResult = require('./dependency-result');
const ActorPath = require('z-abs-corelayer-server/path/actor-path');
const ActorPathData = require('z-abs-corelayer-server/path/actor-path-data');
const ActorPathGenerated = require('z-abs-corelayer-server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');
const Fs = require('fs');
const Path = require('path');
const Npm = require('npm');
const Semver = require('semver');


class DependenciesAnalyzeUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
    this.topDependencies = new Map();
    this.resultArray = [];
    this.actorJsLicenses = [];
  }
  
  onRequest() {
    let pendingList = true;
    let pendingOutdated = true;
    let actorJsDependenciesCache = true;
    let pendingActorJsLicenses = true;
    let pendingNpmView = 0;
    let savedInfos = [];
    let actorJsDependencies = null;
    Npm.load({}, (errNpm, npm) => {
      if(!errNpm) {
        npm.commands.list([], true, (errNpmList, pkgInfo) => {
          pendingList = false;
          const licenses = [];
          if(!errNpmList) {
            const names = Object.keys(pkgInfo.dependencies);
            for(let i in names) {
              this.analyzeDependencies(licenses, pkgInfo.dependencies[names[i]], names[i], pkgInfo.dependencies[names[i]].version, new DependencyResult('package.json'));
            }
            if(!pendingOutdated && 0 !== savedInfos.length) {
              this.analyzeOutdated(savedInfos);
            }
            if(!actorJsDependenciesCache && null !== actorJsDependencies) {
              this.analyzeActorJsDependencies(actorJsDependencies);
            }
            licenses.sort((a, b) => {
              if(a.name < b.name) {
                return -1;
              }
              else if(b.name < a.name) {
                return 1;
              }
              else {
                return 0;
              }
            });
            let x = 1;
            licenses.forEach((lic) => {
              if(undefined === lic.license && undefined == lic.licenses) {
                ++pendingNpmView;
                npm.commands.view([`${lic.name}@${lic.version}`, 'license', 'licenses'], true, (errNpmView, data) => {
                  if(!errNpmView) {
                    const newLicenses = [];
                    const names = Object.keys(data);
                    for(let i in names) {
                      newLicenses.push(data[names[i]].license);
                    }
                    if(0 !== newLicenses.length) {
                      const dependency = this.getTopDependency(lic.name, lic.version);
                      dependency.addLicenses(newLicenses);
                    }
                  }
                  --pendingNpmView;
                  if(!pendingList && !pendingOutdated && !actorJsDependenciesCache && !pendingActorJsLicenses && 0 === pendingNpmView) {
                    this._sortAndSave();
                  }
                });
              }
            });
          }
          if(!pendingList && !pendingOutdated && !actorJsDependenciesCache && !pendingActorJsLicenses && 0 === pendingNpmView) {
            this._sortAndSave();
          }
        });
        npm.commands.outdated([], true, (errNpmOutdated, infos) => {
          pendingOutdated = false;
          if(!errNpmOutdated) {
            if(!pendingList) {
              this.analyzeOutdated(infos);
              if(!actorJsDependenciesCache && null !== actorJsDependencies) {
                this.analyzeActorJsDependencies(actorJsDependencies);
              }
            }
            else {
              savedInfos = infos;
            }
          }  
          if(!pendingList && !pendingOutdated && !actorJsDependenciesCache && !pendingActorJsLicenses && 0 === pendingNpmView) {
            this._sortAndSave();
          }
        });
      }
      else {
        this._sortAndSave();
      }
    });
    Fs.readFile(ActorPathData.getDependenciesFile(), (errFs, data) => {
      actorJsDependenciesCache = false;
      if(!errFs) {
        actorJsDependencies = JSON.parse(data);
        if(!pendingList) {
          this.analyzeActorJsDependencies(actorJsDependencies);
          if(!pendingOutdated && 0 !== savedInfos.length) {
            this.analyzeOutdated(savedInfos);
          }
        }
      }
      if(!pendingList && !pendingOutdated && !actorJsDependenciesCache && !pendingActorJsLicenses && 0 === pendingNpmView) {
        this._sortAndSave();
      }
    });
    Fs.readFile(ActorPathData.getLicensesFile(), (err, data) => {
      pendingActorJsLicenses = false;
      if(!err) {
        this.actorJsLicenses = JSON.parse(data);
      }
      if(!pendingList && !pendingOutdated && !actorJsDependenciesCache && !pendingActorJsLicenses && 0 === pendingNpmView) {
        this._sortAndSave();
      }
    });
  }
  
  getTopDependency(name, version) {
    const dependency = this.topDependencies.get(name);
    if(undefined !== dependency) {
      return dependency.get(version);
    }
  }
  
  getRequiredTopDependency(name, version) {
    const dependency = this.topDependencies.get(name);
    if(undefined !== dependency) {
      const satisfied = [];
      dependency.forEach((v, keyVersion) => {
        if(Semver.satisfies(keyVersion, version)) {
          satisfied.push(v);
        }
      });
      if(1 === satisfied.length) {
        return satisfied[0];
      }
      else if(2 <= satisfied.length) {
        // TODO:
      }
    }
  }
  
  setTopDependency(name, version, dependecyResult) {
    const dependency = this.topDependencies.get(name);
    if(undefined === dependency) {
      this.topDependencies.set(name, new Map([[version, dependecyResult]]));
    }
    else {
      const dr = dependency.get(version);
      if(undefined === dr) {
        dependency.set(version, dependecyResult);
      }
    }
  }
  
  analyzeActorJsDependencies(dependencies) {
    dependencies.forEach((dependency) => {
      const dependecyResults = this.topDependencies.get(dependency.name);
      if(undefined !== dependecyResults) {
        for(let d of dependecyResults) {
          if(-1 !== d[1].parents.indexOf('package.json')) {
            d[1].versionActorJs = dependency.version;
          }
        }
      }
    });
    /*
	const names = Object.keys(dependencies);
    for(let i in names) {
      const name = names[i];
      const version = dependencies[name];
      const dependecyResults = this.topDependencies.get(name);
      if(undefined !== dependecyResults) {
        for(let d of dependecyResults) {
          if(-1 !== d[1].parents.indexOf('package.json')) {
            d[1].versionActorJs = version;
          }
        }
      }
    }*/
  }
  
  analyzeOutdated(infos) {
    infos.forEach((info) => {
      const name = info[1];
      const current = info[2];
      const wanted = info[3];
      const latest = info[4];
      const dependecyResult = this.getTopDependency(name, current ? current : 'MISSING');
      if(undefined !== dependecyResult) {
        dependecyResult.addVersions(wanted, latest);
      }
      else {
        console.log('analyzeOutdated', name, current, wanted, latest);
      }
    });
  }
  
  analyzeDependencies(licenses, dependency, name, versionRequired, parent, depth = 0) {
    let dependecyResult = this.getRequiredTopDependency(name, dependency.version);
    if(undefined === dependecyResult) {
      /*if(dependency.missing && (undefined === dependency.name || '' === dependency.name)) {
        console.log('analyzeDependencies', name, dependency);
        licenses.push({
          name: name,
          version: versionRequired,
          license: undefined,
          licenses: undefined
        });
        dependecyResult = new DependencyResult(name, versionRequired, versionRequired, 'version', undefined, undefined, parent.name);
      }
      else {*/
      if(undefined !== name && '' !== name) {
        licenses.push({
          name: name,
          version: dependency.version,
          license: dependency.license,
          licenses: dependency.licenses
        });
      }
      else {
        console.log(dependency, name, versionRequired, parent);
      }
      dependecyResult = new DependencyResult(name, dependency.version, versionRequired, dependency._requested.type, dependency.license, dependency.licenses, parent.name);
      //}
      this.resultArray.push(dependecyResult);
      this.setTopDependency(dependecyResult.name, dependecyResult.versionCurrent, dependecyResult);
    }
    else {
      dependecyResult.addParentUniqueVersionRequired(parent, versionRequired);
    }
    parent.dependencies.push(name);
    
    if(undefined !== dependency.dependencies) {
      const dependencyNames = Object.keys(dependency.dependencies);
      const _names = Object.keys(dependency._dependencies);
      for(let i in dependencyNames) {
        const dependencyName = dependencyNames[i];
        this.analyzeDependencies(licenses, dependency.dependencies[dependencyName], dependencyName, dependency._dependencies[_names[i]], dependecyResult, depth + 1);
      }
    }
  }
  
  _sortVersion(aVersion, bVersion) {
    const aVersionCurrent = aVersion.split('.');
    const bVersionCurrent = bVersion.split('.');
    const aMajor = Number.parseInt(aVersionCurrent[0]);
    const bMajor = Number.parseInt(bVersionCurrent[0]);
    if(aMajor < bMajor) {
      return -1;
    }
    else if(bMajor < aMajor) {
      return 1;
    }
    else {
      const aMinor = Number.parseInt(aVersionCurrent[1]);
      const bMinor = Number.parseInt(bVersionCurrent[1]);
      if(aMinor < bMinor) {
        return -1;
      }
      else if(bMinor < aMinor) {
        return 1;
      }
      else {
        const aPatch = Number.parseInt(aVersionCurrent[2]);
        const bPatch = Number.parseInt(bVersionCurrent[2]);
        if(aPatch < bPatch) {
          return -1;
        }
        else if(bPatch < aPatch) {
          return 1;
        }
        else {
          return 0;
        }
      }
    }
  }
  
  _sortAndSave() {
    const resultArray = this.resultArray;
    for(let i = 0; i < resultArray.length; ++i) {
      const dependencyResult = resultArray[i];
      if('' === dependencyResult.versionLatest) {
        if('' !== dependencyResult.versionActorJs) {
          dependencyResult.versionLatest = dependencyResult.versionCurrent;
        }
      }
      if('' !== dependencyResult.versionActorJs) {
        dependencyResult.differActorJs = dependencyResult.versionLatest !== dependencyResult.versionActorJs;
      }
      if('' === dependencyResult.versionWanted) {
        dependencyResult.versionWanted = dependencyResult.versionCurrent;
      }
      else {
        dependencyResult.differWanted = dependencyResult.versionActorJs !== dependencyResult.versionWanted;
      }
      dependencyResult.differCurrent = dependencyResult.versionWanted !== dependencyResult.versionCurrent;
    }
    
    resultArray.sort((a, b) => {
      if(1 <= a.parents.length && -1 !== a.parents.indexOf('package.json')) {
        if(1 <= b.parents.length && -1 !== b.parents.indexOf('package.json')) {
          if(a.name < b.name) {
            return -1;
          }
          else if(b.name < a.name) {
            return 1;
          }
          else {
            return this._sortVersion(a.versionCurrent, b.versionCurrent);
          }
        }
        else {
          return -1;
        }
      }
      else if(1 <= b.parents.length && -1 !== b.parents.indexOf('package.json')) {
        return 1;
      }
      else {
        if(a.name < b.name) {
          return -1;
        }
        else if(b.name < a.name) {
          return 1;
        }
        else {
          return this._sortVersion(a.versionCurrent, b.versionCurrent);
        }
      }
    });
    this.asynchWriteFileResponse(ActorPathGenerated.getDependenciesFile(), {
      time: new Date(Date.now()).toUTCString(),
      dependencies: resultArray,
      licenses: this.actorJsLicenses
    }, true);
  }
}


module.exports = DependenciesAnalyzeUpdate;
