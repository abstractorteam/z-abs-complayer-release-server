
'use strict';

const ActorPathData = require('z-abs-corelayer-server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class DependenciesUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(dependencies) {
    this.asynchWriteFileResponse(ActorPathData.getDependenciesFile(), dependencies, true);
  }
}

module.exports = DependenciesUpdate;
