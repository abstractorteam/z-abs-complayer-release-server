
'use strict';

const ActorPathData = require('z-abs-corelayer-server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class LicensesUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(licenses) {
    this.asynchWriteFileResponse(ActorPathData.getLicensesFile(), licenses, true);
  }
}

module.exports = LicensesUpdate;
